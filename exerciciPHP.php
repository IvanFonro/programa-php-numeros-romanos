<html>
<head>
 <title>Vocales en N&uacute;meros Romanos</title>
 <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<div id="contenido">
    	<center>
		<div id="formulario">
<form action="" method="post">
    	<label id="intro" for="frase">Escriba una frase. Se le calcular&aacute el n&uacute;mero total de vocales en n&uacute;meros romanos:</label><br>
    	<input id="introducir" type="text" name="frase" id="frase">
		<input id="btn" type="submit" name="submit" value="Calcular" />
    </form>
		</div>
        <hr>
        </center>
		<div id="result">
<?php
$frase=$_POST['frase'];
/*Declaracion de Funciones*/
function substr_count_array( $frase ) {
     $vocals = 0;
	 
	 $buscamos=array('a','e','i','o','u','A','E','O','I','U');
     foreach ($buscamos as $valor) {
		 
          $vocals += substr_count( $frase, $valor);
		  
     }
     return $vocals;
}

function convertirNum($numero) 
  {
   /** intval(xxx) para que convierta a int **/
   $n = intval($numero);
   $res = '';
   
   /*** Array con los numeros romanos  ***/
   $roman_numerals = array(
      'M'  => 1000,
      'CM' => 900,
      'D'  => 500,
      'CD' => 400,
      'C'  => 100,
      'XC' => 90,
      'L'  => 50,
      'XL' => 40,
      'X'  => 10,
      'IX' => 9,
      'V'  => 5,
      'IV' => 4,
      'I'  => 1);
   
   foreach ($roman_numerals as $roman => $number) 
   {
    /** Dividir para encontrar resultados en array **/
    $matches = intval($n / $number);
   
    /** Asignar el numero romano al resultado **/
    $res .= str_repeat($roman, $matches);
   
    /** Descontar el numero romano al total **/
    $n = $n % $number;
   }
   
   /** Res = String **/
   return $res;
  }

/*Uso de Funciones y resultado*/

$numerovocales = substr_count_array($frase);

$romano = convertirNum($numerovocales); 

if (!$romano) {
	if($frase == null){
	echo "<p>Introduzca una frase.</p>";
	}else{
	echo "<p>En la siguiente introducci&oacuten: ".$frase."</p>";
    echo "<p>No existen vocales.</p>";
	}
} else {
	
echo "<p>En la siguiente frase: ".$frase."</p>";
	
echo "<p>El n&uacute;mero total de vocales es '$romano'.</p>";
}

?>
		</div>
	</div>
</body>
</html>